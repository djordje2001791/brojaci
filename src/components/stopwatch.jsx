import React, {Component} from 'react';
import PropTypes from 'prop-types';

export default class Stopwatch extends Component {
    numberStyle = {
        fontSize: 20
    };

    divStyle = {
        width: 280,
        height: 140
    };

    titleStyle = {
        background: 'none',
        border: 'none',
        cursor: 'pointer',
        height: 50,
        width: this.divStyle.width - 10,
        fontSize: 19,
        color: 'white',
        fontWeight: 'bold'
    };

    titleEditStyle = {
        backgroundColor: 'inherit',
        color: this.titleStyle.color,
        fontWeight: this.titleStyle.fontWeight,
        border: '0 none transparent ',
        width: this.divStyle.width - 35,
        height: 38,
        fontSize: this.titleStyle.fontSize,
        textAlign: 'center',
        boxSizing: 'content-box'
    };

    constructor(props) {
        super(props);

        this.state = {
            hours: 0,
            minutes: 0,
            seconds: 0,
            tents: 0,
            title: this.props.children,
            running: false,
            editingTitle: false,
            stopwatch: null
        };

        this.onStart = this.onStart.bind(this);
        this.onPause = this.onPause.bind(this);
        this.onStop = this.onStop.bind(this);
        this.updateTime = this.updateTime.bind(this);
        this.changeTitleToInput = this.changeTitleToInput.bind(this);
        this.checkChangingToText = this.checkChangingToText.bind(this);
    }

    updateTime() {
        console.log("updating time");

        this.setState({
            tents: this.state.tents + 1,
        });

        if(this.state.tents >= 10)
            this.setState({
                tents: 0,
                seconds: this.state.seconds + 1
            });

        if(this.state.seconds >= 60)
            this.setState({
                seconds: 0,
                minutes: this.state.minutes + 1
            });

        if(this.state.minutes >= 60)
            this.setState({
                minutes: 0,
                hours: this.state.hours + 1
            });
    }

    onStart() {
        this.setState({
            running: true,
            stopwatch: setInterval(this.updateTime, 100)
        });
    }

    onPause() {
        this.setState({
            running: false
        });

        clearInterval(this.state.stopwatch)
    }

    onStop() {
        this.setState({
            hours: 0,
            minutes: 0,
            seconds: 0,
            tents: 0,
            running: false,
            editingTitle: false
        });

        clearInterval(this.state.stopwatch)
    }

    changeTitleToInput() {
        this.setState({
            editingTitle: true
        });
    }

    checkChangingToText(event) {
        if (event.keyCode === 13)
            this.setState({
                title: event.target.value,
                editingTitle: false
            });
    }

    getTitleField() {
        let titleText = <input type='button' onClick={this.changeTitleToInput} style={this.titleStyle} value={this.state.title}/>;
        let titleEdit = <input type='text' maxLength='12' className="form-control default" style={this.titleEditStyle}
                            onKeyUp={this.checkChangingToText} onBlur={this.checkChangingToText}/>;

        return this.state.editingTitle ? titleEdit : titleText;
    }

    getStartButton() {
        let startButton = <input type='button' onClick={this.onStart} className='btn btn-success m-2 btn-sm' value='Покрени'/>;
        let pauseButton = <input type='button' onClick={this.onPause} className='btn btn-warning m-2 btn-sm' value='Паузирај'/>;

        return this.state.running ? pauseButton : startButton;
    }

    isResetButtonDisabled() {
        return !this.state.running &&
            this.state.hours === 0 &&
            this.state.minutes === 0 &&
            this.state.minutes === 0 &&
            this.state.seconds === 0 &&
            this.state.tents === 0;
    }

    getTimeText() {
        return ('0' + this.state.hours).slice(-2) + ':' +
            ('0' + this.state.minutes).slice(-2) + ':' +
            ('0' + this.state.seconds).slice(-2) + ':' +
            String(this.state.tents);
    }

    render() {
        return (<div style={this.divStyle} className='badge m-1 badge-info'>
            {this.getTitleField()}

            <p style={this.numberStyle}>{this.getTimeText()}</p>

            {this.getStartButton()}
            <input type='button' onClick={this.onStop} className='btn btn-danger m-2 btn-sm' disabled={this.isResetButtonDisabled()} value='Ресетуј'/>
            <input type='button' onClick={this.props.removeAction} className='btn btn-danger m-2 btn-sm' value='Избриши'/>
        </div>);
    }
}

Stopwatch.propTypes = {
    start: PropTypes.number,
    removeAction: PropTypes.func,
    title: PropTypes.string
};