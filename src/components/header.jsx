import React, {Component, Fragment} from 'react';
import PropTypes from 'prop-types';

export default class Header extends Component {
    getCorrectText() {
        if(this.props.count === 0)
            return 'Нема бројача';

        return 'Укупно' +
            (this.props.count % 10 < 2 || (this.props.count <= 20 && this.props.count > 4)  ? ' има ' : ' имају ') +
            String(this.props.count) +
            (this.props.count % 10 === 1 && this.props.count !== 11 ? ' бројач' : ' бројача');
    }

    canDeleteElements() {
        return this.props.count === 0;
    }

    render() {
        return (<Fragment>
            <nav className='navbar navbar-dark bg-dark'>
                <span className='navbar-brand mb-0 h1'>{this.getCorrectText()}</span>

                <div>
                    <div className='btn-group'>
                        <button type='button' className='btn btn-success dropdown-toggle' data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                            Додај
                        </button>

                        <div className='dropdown-menu' aria-labelledby='dropdownMenuButton'>
                            <input type='button' onClick={this.props.addCounter} className='dropdown-item' value='Бројач'/>
                            <input type='button' onClick={this.props.addStopwatch} className='dropdown-item' value='Штоперица'/>
                        </div>
                    </div>

                    <input type='button' onClick={this.props.removeAllElements} className='btn btn-danger m-1'
                           disabled={this.canDeleteElements()} value='Избриши све бројаче'/>
                </div>
            </nav>
        </Fragment>);
    }
}

Header.propTypes = {
    count: PropTypes.number,
    addCounter: PropTypes.func,
    addStopwatch: PropTypes.func,
    removeAllElements: PropTypes.func
};

