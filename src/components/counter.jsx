import React, {Component} from 'react';
import PropTypes from 'prop-types';

export default class Counter extends Component {
    numberStyle = {
        fontSize: 20
    };

    divStyle = {
        width: 280,
        height: 140
    };

    titleStyle = {
        background: 'none',
        border: 'none',
        cursor: 'pointer',
        height: 50,
        width: this.divStyle.width - 10,
        fontSize: 19,
        color: 'white',
        fontWeight: 'bold'
    };

    titleEditStyle = {
        backgroundColor: 'inherit',
        color: this.titleStyle.color,
        fontWeight: this.titleStyle.fontWeight,
        border: '0 none transparent ',
        width: this.divStyle.width - 35,
        height: 38,
        fontSize: this.titleStyle.fontSize,
        textAlign: 'center',
        boxSizing: 'content-box'
    };

    constructor(props) {
        super(props);

        this.state = {
            count: this.props.start,
            title: this.props.children,
            editingTitle: false
        };

        this.doIncrement = this.doIncrement.bind(this);
        this.doDecrement = this.doDecrement.bind(this);
        this.changeTitleToInput = this.changeTitleToInput.bind(this);
        this.checkChangingToText = this.checkChangingToText.bind(this);
    }

    getCount() {
        return this.state.count === 0 ? 'Нула' : this.state.count;
    }

    doIncrement() {
        this.setState({
            count: this.state.count + 1
        });
    }

    doDecrement() {
        this.setState({
            count: this.state.count - 1
        });
    }

    canDecrement() {
        return this.state.count === 0;
    }

    changeTitleToInput() {
        this.setState({
            editingTitle: true
        });
    }

    checkChangingToText(event) {
        if (event.keyCode === 13)
            this.setState({
                title: event.target.value,
                editingTitle: false
            });
    }

    getTitleField() {
        let titleText = <input type='button' onClick={this.changeTitleToInput} style={this.titleStyle} value={this.state.title}/>;
        let titleEdit = <input type='text' maxLength='12' className="form-control default" style={this.titleEditStyle}
                            onKeyUp={this.checkChangingToText} onBlur={this.checkChangingToText}/>;

        return this.state.editingTitle ? titleEdit : titleText;
    }

    render() {
        return (<div style={this.divStyle} className='badge m-1 badge-primary'>
            {this.getTitleField()}

            <p style={this.numberStyle}>{this.getCount()}</p>

            <input type='button' onClick={this.doIncrement} className='btn btn-secondary m-2 btn-sm' value='Повећај'/>
            <input type='button' onClick={this.doDecrement} className='btn btn-secondary m-2 btn-sm' disabled={this.canDecrement()} value='Смањи'/>
            <input type='button' onClick={this.props.removeAction} className='btn btn-danger m-2 btn-sm' value='Избриши'/>
        </div>);
    }
}

Counter.propTypes = {
    start: PropTypes.number,
    removeAction: PropTypes.func,
    title: PropTypes.string
};