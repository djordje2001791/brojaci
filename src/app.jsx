import React, {Component, Fragment} from 'react';

import Counter from './components/counter';
import Stopwatch from './components/stopwatch';
import Header from './components/header';

export default class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            elements: [
                {type: 'counter', key: 1, start: 5, title: '1. бројач'},
                {type: 'stopwatch', key: 2, title: '2. штоперица'}
            ]
        };

        this.addCounter = this.addCounter.bind(this);
        this.addStopwatch = this.addStopwatch.bind(this);
        this.removeElement = this.removeElement.bind(this);
        this.removeAllElements = this.removeAllElements.bind(this);
    }

    addCounter() {
        const newElementKey = this.state.elements.length > 0 ? this.state.elements[this.state.elements.length - 1].key + 1 : 1;

        console.log('Adding counter with key = ' + newElementKey);

        this.setState({
            elements: [...this.state.elements,
                {type: 'counter', key: newElementKey, start: 0, title: newElementKey + '. бројач'}
            ]
        });
    }

    addStopwatch() {
        const newElementKey = this.state.elements.length > 0 ? this.state.elements[this.state.elements.length - 1].key + 1 : 1;

        console.log('Adding stopwatch with key = ' + newElementKey);

        this.setState({
            elements: [...this.state.elements,
                {type: 'stopwatch', key: newElementKey, title: newElementKey + '. штоперица'}
            ]
        });
    }

    removeElement(key) {
        console.log('Deleting element with key = ' + key);

        this.setState({
            elements: this.state.elements.filter(value => {return value.key !== key;})
        });
    }

    removeAllElements(){
        console.log('Deleting all elements');

        this.setState({
            elements: []
        });
    }

    render() {
        return (<Fragment>
            <Header count={this.state.elements.length} addCounter={this.addCounter} addStopwatch={this.addStopwatch}
                    removeAllElements={this.removeAllElements}/>

            {this.state.elements.map(elements => elements.type === 'counter' ?
                <Counter key={elements.key} start={elements.start} removeAction={() => this.removeElement(elements.key)}>
                    {elements.title}
                </Counter>
                :
                <Stopwatch key={elements.key} removeAction={() => this.removeElement(elements.key)}>
                    {elements.title}
                </Stopwatch>
            )}
        </Fragment>);
    }
}